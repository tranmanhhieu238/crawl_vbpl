import time
from requests_html import HTMLSession
import os
import zipfile
import wget
import shutil
from requests.adapters import HTTPAdapter
from urllib3.util.retry import Retry

def dowloadFile(url_pdf, file_location):
    attempt = 1
    success = False
    while attempt <= 10 and not success:
        try:
            file = wget.download(url_pdf, file_location)
            print(f"File downloaded: {file}")
            success = True
            return True
        except Exception as e:
            print(f"Attempt {attempt} failed. Error: {str(e)}")
            attempt += 1

    if not success:
        print(f"Failed to download file after 10 attempts.")
        return False


def _extract_file(folder_path):
    for file_name in os.listdir(folder_path):
        file_path = os.path.join(folder_path, file_name)
        # Kiểm tra xem tệp tin có phải là file zip hay không
        if file_name.lower().endswith('.zip'):
            # Giải nén tệp tin zip
            with zipfile.ZipFile(file_path, 'r') as zip_ref:
                zip_ref.extractall(folder_path)

            # Xóa tệp tin zip sau khi giải nén
            os.remove(file_path)

            print(f"Đã giải nén và xóa tệp tin {file_name}")


def get_dow_link(keyword, xpath_folder):
    session = HTMLSession()
    retry = Retry(connect=3, backoff_factor=0.5)
    adapter = HTTPAdapter(max_retries=retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)
    r = session.get(f'https://vbpl.vn/pages/vbpq-timkiem.aspx?type=0&s=1&SearchIn=Title,Title1&Keyword={keyword}')

    r.html.render()  # this call executes the js in the page
    li_url = r.html.find('#A2')
    print(li_url)
    #Nếu không có diglog
    if len(li_url) ==0:
        li_url = r.html.xpath('//li[@class="download"]/a')
        print(li_url)
    print(keyword)
    for i in li_url:    
        item = i.attrs['href'].split("'")
        name = item[1]
        url = item[3]
        dowload_url = 'https://vbpl.vn' + url
        print(name + '-------' + dowload_url)
        if ('pdf' in name) or ('.doc' in name):
            # Đường dẫn đến location lưu file
            xpath_file = xpath_folder
            xpath_f = os.path.dirname(xpath_file)
            location = os.path.join(xpath_f, name[:-4])
            print(location)
            if os.path.exists(location):
                print("Thư mục đã tồn tại.")
            else:
                # Tạo thư mục nếu chưa tồn tại
                os.mkdir(location)
            print(dowload_url)
            boo = dowloadFile(dowload_url, location)
            if boo:
                break
            if not os.listdir(location):
                # Nếu không có file nào, xóa thư mục
                shutil.rmtree(location)
                print(f"Thư mục {location} đã được xóa.")


if __name__ == '__main__':

    # Nơi lưu trữ file văn bản
    xpath_folder = 'D:\CMC project\\vbpl\\vbpl_key\\'

    with open('list_legal_ids.txt', 'r', encoding='UTF-8') as f:
        data = f.readlines()
    list_keyword = [i.replace('\n', '') for i in data]
    print(list_keyword)
    for kw in list_keyword:
        kw = kw.strip().replace(' ', '%20')
        get_dow_link(kw, xpath_folder)
        time.sleep(3)
    # keyword = 'Quyết định 20/2020/QĐ-UBND'