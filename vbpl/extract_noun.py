# import pandas as pd
#
# # Đọc dữ liệu từ file .xlsx ban đầu
# file_path = "D:\\CMC project\\vbpl\\counter.xlsx"
# df = pd.read_excel(file_path)
#
# # Lọc các dòng có giá trị của cột "pos" là "N"
# filtered_df = df[df["pos"] == "N"]
#
# # Lưu các dòng đã lọc vào file .xlsx mới
# output_file_path = "D:\\CMC project\\vbpl\\extract_noun.xlsx"
# filtered_df.to_excel(output_file_path, index=False, engine="openpyxl")
import pandas as pd

# Đường dẫn tới file .xlsx
file_path = "D:\\CMC project\\vbpl\\counter.xlsx"

# Đọc dữ liệu từ 3 sheet "unigram", "bigram", và "trigram"
sheet_names = ["unigram", "bigram", "trigram"]
dfs = pd.read_excel(file_path, sheet_name=sheet_names)

# Lọc các dòng có giá trị của cột "pos" là "N" cho từng sheet
filtered_dfs = {}
for sheet_name in sheet_names:
    df = dfs[sheet_name]
    filtered_df = df[df["pos"] == "N"]
    filtered_dfs[sheet_name] = filtered_df

# Tạo file_output.xlsx mới và lưu 3 sheet có tên tương tự
output_file_path = "D:\\CMC project\\vbpl\\extract_noun.xlsx"
with pd.ExcelWriter(output_file_path, engine="openpyxl") as writer:
    for sheet_name, filtered_df in filtered_dfs.items():
        filtered_df.to_excel(writer, sheet_name=sheet_name, index=False)

    # Lưu dữ liệu vào file_output.xlsx trước khi đóng writer
    writer.save()