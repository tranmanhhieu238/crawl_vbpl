import os
import pymongo
import config
import wget
import shutil
import zipfile


def _dowloadFile(url_pdf, file_location):
    attempt = 1
    success = False
    print(url_pdf)
    while attempt <= 10 and not success:
        try:
            file = wget.download(url_pdf, file_location)
            print(f"File downloaded: {file}")
            success = True
            _extract_file(file_location)
        except Exception as e:
            print(f"Attempt {attempt} failed. Error: {str(e)}")
            attempt += 1

    if not success:
        print(f"Failed to download file after 10 attempts.")


def _extract_file(folder_path):
    for file_name in os.listdir(folder_path):
        file_path = os.path.join(folder_path, file_name)

        # Kiểm tra xem tệp tin có phải là file zip hay không
        if file_name.lower().endswith('.zip'):
            # Giải nén tệp tin zip
            with zipfile.ZipFile(file_path, 'r') as zip_ref:
                zip_ref.extractall(folder_path)

            # Xóa tệp tin zip sau khi giải nén
            os.remove(file_path)

            print(f"Đã giải nén và xóa tệp tin {file_name}")


def _getUrl_file():
    uri = f"mongodb://{config.username}:{config.password}@localhost:{config.port}"
    client = pymongo.MongoClient(uri)
    database = client['vbpl']
    collection = database['vbpl']
    url_files = collection.distinct('files')
    print(len(url_files))
    for url_file in url_files:

        url_file = url_file.replace('[', '').replace(']', '').replace('{', '').replace('}', '').replace(',', '')
        url_file = url_file.split('"')

        if len(url_file) > 3:
            # Đường dẫn đến location lưu file
            xpath_file = 'D:\CMC project\\vbpl\\vbpl1\\'
            xpath_f = os.path.dirname(xpath_file)
            location = os.path.join(xpath_f, url_file[7][:-4].strip())
            print(location)
            if os.path.exists(location):
                print("Thư mục đã tồn tại.")
            else:
                # Tạo thư mục nếu chưa tồn tại
                os.mkdir(location)
            print(len(url_file))
            for li in url_file:
                if 'https://vbpl' in li:
                    _dowloadFile(li, location)
                    print(li)
            if not os.listdir(location):
                # Nếu không có file nào, xóa thư mục
                shutil.rmtree(location)
                print(f"Thư mục {location} đã được xóa.")


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    _getUrl_file()
    # _dowloadFile('https://vbpl.vn/FileData/TW/Lists/vbpq/Attachments/25854/Phu luc 4.zip','VanBanGoc_44_2010_QH12')
