import seleniumwire.undetected_chromedriver.v2 as uc
import chromedriver_autoinstaller
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
import random
import time
from datetime import datetime
import os
import pymongo
import config
import wget
import requests
import shutil
import pickle


def _get_cookie(driver):
    driver.get('https://thuvienphapluat.vn/')
    username = driver.find_element(By.ID, "usernameTextBox")
    username.send_keys("bandautu")
    time.sleep(0.5)

    password = driver.find_element(By.ID, "passwordTextBox")
    password.send_keys("bandautu")
    time.sleep(0.5)

    sign_in = driver.find_element(By.ID, 'loginButton')
    sign_in.click()

    time.sleep(0.5)
    confirm_login = driver.find_element(By.XPATH,
                                        "//div[@aria-labelledby='ui-dialog-title-logintfrom_w']/div[contains(@class,'ui-widget-content ui-helper-clearfix')]/div/button[1]")
    confirm_login.click()
    time.sleep(1)
    pickle.dump(driver.get_cookies(), open("cookies.pkl", "wb"))


def _getUrlDocument(keyWord,driver):
    driver.get('https://thuvienphapluat.vn/')
    time.sleep(0.5)
    cookies = pickle.load(open("cookies.pkl", "rb"))
    for cookie in cookies:
        driver.add_cookie(cookie)
    time.sleep(0.5)
    driver.get('https://thuvienphapluat.vn/')
    # username = driver.find_element(By.ID, "usernameTextBox")
    # username.send_keys("bandautu")
    # time.sleep(0.5)
    #
    # password = driver.find_element(By.ID, "passwordTextBox")
    # password.send_keys("bandautu")
    # time.sleep(0.5)
    #
    # sign_in = driver.find_element(By.ID, 'loginButton')
    # sign_in.click()
    #
    # time.sleep(0.5)
    # confirm_login = driver.find_element(By.XPATH,
    #                                     "//div[@aria-labelledby='ui-dialog-title-logintfrom_w']/div[contains(@class,'ui-widget-content ui-helper-clearfix')]/div/button[1]")
    # confirm_login.click()
    # time.sleep(1)

    keyword = driver.find_element(By.ID, 'txtKeyWord')
    keyword.send_keys(keyWord)

    enter_search = driver.find_element(By.ID, 'btnKeyWordHome')
    enter_search.click()

    time.sleep(1)

    dow_button = driver.find_element(By.XPATH,
                                     '(//div[@id="block-info-advan"]/*/div[@class="content-0"])[1]/div[@class="left-col"]/div[@class="nq"]/p[@class="links-bot"]/a[@onclick="Doc_DL(MemberGA)"]')
    dow_button.click()

    time.sleep(1)

    list_href = []
    dowload_href = driver.find_elements(By.XPATH, '//div[@class="download"]/p/a')
    for link in dowload_href:
        if link.get_attribute('href'):
            list_href.append(link.get_attribute('href'))
    for link in list_href:
        driver.get(link)
        time.sleep(3.5)

        location = 'C:/Users/adm/csdlpt Dropbox/Hieu Tran/PC/Downloads'

        # Lấy danh sách tất cả các file trong thư mục đích
        files = os.listdir(location)

        # Xác định file vừa được tải về dựa trên thời gian tạo file
        downloaded_file = None
        latest_timestamp = 0
        for file in files:
            file_path = os.path.join(location, file)
            if os.path.isfile(file_path):
                timestamp = os.path.getctime(file_path)
                if timestamp > latest_timestamp:
                    latest_timestamp = timestamp
                    downloaded_file = file

        # Kiểm tra xem có file vừa tải về hay không
        if downloaded_file:
            downloaded_file_path = os.path.join(location, downloaded_file)

            print("File vừa tải về được tìm thấy: ", downloaded_file_path)

            # Đường dẫn đích
            destination_path = _createFolderToSaveFile(keyWord)

            # Di chuyển file
            try:
                shutil.move(downloaded_file_path, destination_path)
                print("File đã được di chuyển thành công.")

            except:
                pass
        else:
            print("Không tìm thấy file vừa tải về.")
    driver.quit()


def _createFolderToSaveFile(keyword):
    name_file = keyword
    if '/' in name_file:
        name_file = name_file.replace('/', '_')
    if '\\' in name_file:
        name_file = name_file.replace('\\', '')
    if ':' in name_file:
        name_file = name_file.replace(':', '')
    xpath_file = 'D:/CMC project/vbpl/vbpl/'
    location = os.path.join(xpath_file, name_file)
    if os.path.exists(location):
        print("Thư mục đã tồn tại.")
    else:
        # Tạo thư mục nếu chưa tồn tại
        os.mkdir(location)
    return location

options = uc.ChromeOptions()
options.add_argument('--ignore-ssl-errors=yes')
options.add_argument('--ignore-certificate-errors')
options.add_argument('--no-first-run --no-service-autorun --password-store=basic')

CHROME_DRIVER_PATH = '/usr/bin/chromedriver'
driver = uc.Chrome(executable_path=CHROME_DRIVER_PATH, options=options)

_get_cookie(driver)

#lấy keyword tìm kiếm
with open('list_legal_ids.txt', 'r', encoding='UTF-8') as f:
    data = f.readlines()
list_keyword = [i.replace('\n', '') for i in data]

for kw in list_keyword:
    kw = kw.strip()
    _getUrlDocument(kw, driver)
    time.sleep(1)
    # print(kw)
    # break
